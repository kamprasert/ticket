-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table ticket.movies
CREATE TABLE IF NOT EXISTS `movies` (
  `id` varchar(50) NOT NULL,
  `name_th` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `running_time` int(3) DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `coming_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table ticket.movies: 6 rows
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` (`id`, `name_th`, `name_en`, `running_time`, `expired_date`, `coming_date`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
	('fd806930-c9c4-45c9-a8d8-f62f14f980ff', 'ท็อปกัน', 'topgun', 130, '2022-10-30', '2022-10-15', '2022-10-09 19:29:57', 'ADMIN', '2022-10-09 19:29:59', 'ADMIN', NULL),
	('a08cbf40-f0b7-49f4-972f-84da2e90530e', 'โดราเอมอน', 'Doraemon', 130, '2022-10-20', '2022-10-08', '2022-10-09 19:29:57', 'ADMIN', '2022-10-09 19:29:59', 'ADMIN', NULL),
	('e68d1376-856b-4d42-9cdd-fd25445af768', 'โดราเอมอน', 'Doraemon', 125, '2022-10-20', '2022-10-08', '2022-10-09 19:29:57', 'ADMIN', '2022-10-09 19:29:59', 'ADMIN', NULL),
	('824fc5f9-25ed-4d52-9281-215ff92f93db', 'ท็อปกัน', 'topgun', 137, '2022-10-30', '2022-10-16', '2022-10-09 19:29:57', 'ADMIN', '2022-10-09 19:29:59', 'ADMIN', NULL),
	('32d5195d-ed76-4e4a-9769-87a1a47fd847', 'โดราเอมอน', 'Doraemon', 120, '2022-10-20', '2022-10-08', '2022-10-09 19:29:57', 'ADMIN', '2022-10-09 19:29:59', 'ADMIN', NULL),
	('7b08d6f4-5871-402b-9c59-4c56b6560910', 'ท็อปกัน', 'topgun', 140, '2022-10-20', '2022-10-08', '2022-10-09 19:29:57', 'ADMIN', '2022-10-09 19:29:59', 'ADMIN', NULL);
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;

-- Dumping structure for table ticket.movie_theater_mapping
CREATE TABLE IF NOT EXISTS `movie_theater_mapping` (
  `id` varchar(50) NOT NULL,
  `theater_id` varchar(50) DEFAULT NULL,
  `movie_id` varchar(50) DEFAULT NULL,
  `price` int(3) DEFAULT NULL,
  `show_date` date DEFAULT NULL,
  `star_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table ticket.movie_theater_mapping: 5 rows
/*!40000 ALTER TABLE `movie_theater_mapping` DISABLE KEYS */;
INSERT INTO `movie_theater_mapping` (`id`, `theater_id`, `movie_id`, `price`, `show_date`, `star_time`, `end_time`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
	('e03a8443-f26d-458a-bd82-24ed4109239d', 'a427212e-f418-4744-ad45-cec0527556ca', 'a08cbf40-f0b7-49f4-972f-84da2e90530e', 200, '2022-10-10', '20:00:00', '22:10:00', '2022-10-09 20:22:32', 'ADMIN', '2022-10-09 20:22:33', 'ADMIN', NULL),
	('e850e6aa-4b3f-4c4b-adff-f659b4f50085', 'a427212e-f418-4744-ad45-cec0527556ca', 'a08cbf40-f0b7-49f4-972f-84da2e90530e', 200, '2022-10-10', '17:00:00', '19:10:00', '2022-10-09 20:22:32', 'ADMIN', '2022-10-09 20:22:33', 'ADMIN', NULL),
	('9813ed3c-3d69-4ecd-827d-4e7449c5505f', 'a427212e-f418-4744-ad45-cec0527556ca', 'a08cbf40-f0b7-49f4-972f-84da2e90530e', 200, '2022-10-10', '12:00:00', '14:10:00', '2022-10-09 20:22:32', 'ADMIN', '2022-10-09 20:22:33', 'ADMIN', NULL),
	('acacbdb7-aa8c-409c-a509-cc0fa01854b3', 'f56e7116-4723-4bd5-a3f7-1243fef7e479', 'a08cbf40-f0b7-49f4-972f-84da2e90530e', 200, '2022-10-10', '12:00:00', '14:10:00', '2022-10-09 20:22:32', 'ADMIN', '2022-10-09 20:22:33', 'ADMIN', NULL),
	('2e3fc806-3ddf-4fc0-ba48-39806c71cab9', 'f56e7116-4723-4bd5-a3f7-1243fef7e479', 'a08cbf40-f0b7-49f4-972f-84da2e90530e', 200, '2022-10-10', '17:00:00', '19:10:00', '2022-10-09 20:22:32', 'ADMIN', '2022-10-09 20:22:33', 'ADMIN', NULL);
/*!40000 ALTER TABLE `movie_theater_mapping` ENABLE KEYS */;

-- Dumping structure for table ticket.theaters
CREATE TABLE IF NOT EXISTS `theaters` (
  `id` varchar(50) NOT NULL,
  `name_th` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table ticket.theaters: 3 rows
/*!40000 ALTER TABLE `theaters` DISABLE KEYS */;
INSERT INTO `theaters` (`id`, `name_th`, `name_en`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	('a427212e-f418-4744-ad45-cec0527556ca', 'โรง 1', 'Theater 1', '2022-10-09 19:38:56', 'ADMIN', '2022-10-09 19:38:56', 'ADMIN'),
	('f56e7116-4723-4bd5-a3f7-1243fef7e479', 'โรง 2', 'Theater 2', '2022-10-09 19:38:56', 'ADMIN', '2022-10-09 19:38:56', 'ADMIN'),
	('e68d1376-856b-4d42-9cdd-fd25445af768', 'โรง 3', 'Theater 3', '2022-10-09 19:38:56', 'ADMIN', '2022-10-09 19:38:56', 'ADMIN');
/*!40000 ALTER TABLE `theaters` ENABLE KEYS */;

-- Dumping structure for table ticket.tickets
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` varchar(50) NOT NULL,
  `movie_theater_mapping_id` varchar(50) DEFAULT NULL,
  `seat_id` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table ticket.tickets: 4 rows
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` (`id`, `movie_theater_mapping_id`, `seat_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	('18fccd31-ae46-4c38-b4ff-b61566f07f26', '2e3fc806-3ddf-4fc0-ba48-39806c71cab9', 'A5', '2022-10-09 22:27:20', 'ADMIN', '2022-10-09 22:27:20', 'ADMIN'),
	('cf8a567f-f18c-4964-b861-92eca0187a0c', '2e3fc806-3ddf-4fc0-ba48-39806c71cab9', 'B5', '2022-10-09 22:27:20', 'ADMIN', '2022-10-09 22:27:20', 'ADMIN'),
	('5862d9c1-9f26-4475-a4f6-81370f608783', '2e3fc806-3ddf-4fc0-ba48-39806c71cab9', 'B6', '2022-10-09 22:27:20', 'ADMIN', '2022-10-09 22:27:20', 'ADMIN'),
	('430fa11a-2c4a-479a-8235-5f2902bba92b', '2e3fc806-3ddf-4fc0-ba48-39806c71cab9', 'A6', '2022-10-09 22:27:20', 'ADMIN', '2022-10-09 22:27:20', 'ADMIN'),
	('c773b92d-4692-4ff5-ba0c-13ca62359e8b', '2e3fc806-3ddf-4fc0-ba48-39806c71cab9', 'C5', '2022-10-09 16:11:49', 'ADMIN', '2022-10-09 16:11:49', 'ADMIN');
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
