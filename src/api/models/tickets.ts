
export interface Ticket {
    id: string;
    movie_theater_mapping_id: string;
    seat_id: string;
    created_at: string;
    created_by: Date;
    updated_at: string;
    updated_by: Date;
}