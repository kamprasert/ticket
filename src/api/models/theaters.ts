export interface Theater {
    id: string;
    name_th: string;
    name_en: string;
    created_at: Date;
    created_by: string;
    updated_at: Date;
    updated_by: string;
}