export interface Moive {
    id: string;
    name_th: string;
    name_en: string;
    running_time: string;
    expired_date: Date;
    coming_date: Date;
    created_at: Date;
    created_by: string;
    updated_at: Date;
    updated_by: string;
    deleted_at: Date;
}