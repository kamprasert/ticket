import express from "express";
import {db} from "../../lib/db";
import { responseBuilder, APIResponse, Empty } from "../../lib/response";

export const movieTheaterMappingRouter = express.Router();

movieTheaterMappingRouter.get<
    Empty,// Params
    APIResponse<Empty>,// ResBody
    Empty,// ReqBody
    {movieId: String, theaterId: String, starTime: String},// ReqQuery
    Empty// Locals
    >
    ("/", async (req, res) => { 
    let {movieId, theaterId, starTime} = req.query
    let queryString = `SELECT mt.*, t.name_th AS theater_name_th, t.name_en AS theater_name_en FROM movie_theater_mapping AS mt JOIN theaters as t ON t.id = theater_id WHERE deleted_at IS NULL AND `
    if(!movieId){
        const response = responseBuilder.badRequest('movieId is required.')
        return res.status(response.status).send(response.payload)
    } else {
        queryString += `movie_id = '${movieId}'`
        if (theaterId) {
            queryString += `AND theater_id = '${theaterId}'`
            if (starTime) {
                queryString += `AND star_time = '${starTime}'`
            }
        }
    }
    db.query(queryString, async (err, result) => {
        if (err) { 
            console.log(err)
            let response = responseBuilder.serverError()
            return res.status(response.status).send(response.payload)
        };
            let response = responseBuilder.success(result)
            return res.status(response.status).send(response.payload)
        }
    );
});

