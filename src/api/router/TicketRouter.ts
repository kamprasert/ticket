import express from "express";
import {v4} from "uuid";
import {db} from "../../lib/db";
import { responseBuilder, APIResponse, Empty} from "../../lib/response";
import { Ticket } from "../models/tickets";

export const ticketRouter = express.Router();


ticketRouter.get<
    Empty,// Params
    APIResponse<Ticket>,// ResBody
    Empty,// ReqBody
    {movieTheaterMappingId: String},// ReqQuery
    Empty// Locals
    >
    ("/", async (req, res) => { 
    let {movieTheaterMappingId} = req.query

    if(!movieTheaterMappingId){
        const response = responseBuilder.badRequest('movieTheaterMappingId is required.')
        return res.status(response.status).send(response.payload)
    }
    const queryMovie = `SELECT * FROM movie_theater_mapping WHERE id  = '${movieTheaterMappingId}'`
    db.query(queryMovie, async (err, result_movie) => {
        if (err) { 
            console.log(err)
            let response = responseBuilder.serverError()
            return res.status(response.status).send(response.payload)
        };
            if (!result_movie.length) {
                let response = responseBuilder.noContent()
                return res.status(response.status).send(response.payload)
            }
            const queryString = `SELECT * FROM tickets WHERE movie_theater_mapping_id  = '${movieTheaterMappingId}' ORDER BY seat_id ASC`
            db.query(queryString, async (err, result) => {
                if (err) { 
                    console.log(err)
                    let response = responseBuilder.serverError()
                    return res.status(response.status).send(response.payload)
                };
                    let response = responseBuilder.success(result)
                    return res.status(response.status).send(response.payload)
                }
            );

        }
    );

    
});

ticketRouter.post<
    Empty,// Params
    APIResponse<Empty>,// ResBody
    { movieTheaterMappingId: String, seat_id: String},// ReqBody
    Empty,// ReqQuery
    Empty// Locals
    >
    ("/", async (req, res) => { 
    const date: Date = new Date()
    let nowDate: string = `${date.getUTCFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getUTCDate()).slice(-2)} ${date.getUTCHours()}:${date.getUTCMinutes()}:${date.getUTCSeconds()}`
    let {movieTheaterMappingId, seat_id} = req.body

    if(!movieTheaterMappingId){
        const response = responseBuilder.badRequest('movieTheaterMappingId is required.')
        return res.status(response.status).send(response.payload)
    }
    if(!seat_id){
        const response = responseBuilder.badRequest('seat_id is required.')
        return res.status(response.status).send(response.payload)
    }

    const queryMovieTheaterMapping = `SELECT * FROM tickets WHERE movie_theater_mapping_id = '${movieTheaterMappingId}' AND seat_id = '${seat_id}'`
    db.query(queryMovieTheaterMapping, async (err, resultMovieTheaterMappingId) => {
        if (err) { 
            console.log(err)
            let response = responseBuilder.serverError()
            return res.status(response.status).send(response.payload)
        };
            if (resultMovieTheaterMappingId.length) {
                let response = responseBuilder.badRequest('seat is duplicate')
                return res.status(response.status).send(response.payload)
            }
            const queryString = `INSERT INTO tickets (id, movie_theater_mapping_id, seat_id, created_by, updated_by, created_at, updated_at) VALUES ('${v4()}', '${movieTheaterMappingId}', '${seat_id}', 'ADMIN', 'ADMIN', '${nowDate}', '${nowDate}')`
            db.query(queryString, async (err, result) => {
                if (err) { 
                    console.log(err)
                    let response = responseBuilder.serverError()
                    return res.status(response.status).send(response.payload)
                };
                    let response = responseBuilder.created()
                    return res.status(response.status).send(response.payload)
                }
            );

        }
    );
});

