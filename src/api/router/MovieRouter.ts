import express, { Request, Response } from "express";
import {db} from "../../lib/db";
import { responseBuilder, APIResponse, Empty } from "../../lib/response";

export const moiveRouter = express.Router();


moiveRouter.get<
    Empty,// Params
    APIResponse<Empty>,// ResBody
    Empty,// ReqBody
    Empty,// ReqQuery
    Empty// Locals
    >
    ("/", async (req, res) => { 
    let date: Date = new Date()
    let nowDate: string = `${date.getUTCFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getUTCDate()).slice(-2)}`
    const queryString = `SELECT * FROM movies WHERE deleted_at IS NULL AND expired_date  > '${nowDate}' AND coming_date  < '${nowDate}'`
    db.query(queryString, async (err, result) => {
        if (err) { 
            let response = responseBuilder.serverError()
            return res.status(response.status).send(response.payload)
        };
            let response = responseBuilder.success(result)
            return res.status(response.status).send(response.payload)
        }
    );
});

