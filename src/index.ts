import express, { Application, Request, Response } from 'express';
import dotenv from 'dotenv';
import {moiveRouter} from "./api/router/MovieRouter";
import {movieTheaterMappingRouter} from "./api/router/MovieTheaterMappingRouter";
import {ticketRouter} from "./api/router/TicketRouter";
dotenv.config();

const app:Application = express();
app.use(express.json());
const port = process.env.APP_PORT;
const url = process.env.APP_URL;

app.get('/', (req: Request, res: Response) => {
    res.send('Express + TypeScript Server');
});

app.use('/api/movie', moiveRouter);
app.use('/api/movie-theater-mapping', movieTheaterMappingRouter);
app.use('/api/ticket', ticketRouter);

app.listen(port, () => {
    console.log(`[server]: Server is running at ${url}:${port}`);
});

