export interface APIResponse<Data> {
    data: Data[] | null , 
    message: String | null , 
    error: String | null
}

export interface Empty {

}

export const responseBuilder = {
    success: (data: any = null) => {
        return { 
            payload: {
                message: 'Success',
                data: data,
                error: null
            },
            status: 200
        }
    },
    created: (data: any = null) => {
        return { 
            payload: {
                message: 'Created Success',
                data: data,
                error: null
            },
            status: 201
        }
    },
    noContent: (data: any = null) => {
        return { 
            payload: {
                message: 'No Content',
                data: data,
                error: null
            },
            status: 204
        }
    },

    badRequest: (message: string|null = null) => {
        return { 
            payload: {
                message: message,
                data: null,
                error: 'Bad Request'
            },
            status: 400
        }
    },

    unauthorized: (message: string|null = null) => {
        return { 
            payload: {
                message: message,
                data: null,
                error: 'Unauthorized'
            },
            status: 401
        }
    },
    
    methodNotAllowed: (message: string|null = null) => {
        return { 
            payload: {
                message: message,
                data: null,
                error: 'Method Not Allowed'
            },
            status: 405
        }
    },
    serverError: () => {
        return { 
            payload: {
                message: 'Error',
                data: null,
                error: 'Internal Server Error'
            },
            status: 500
        }
    },
    serviceUnavailable: () => {
        return { 
            payload: {
                message: 'Error',
                data: null,
                error: 'Service Unavailable'
            },
            status: 503
        }
    },
}
